import pandas
import matplotlib.pyplot as plt
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn import tree

url = "music.csv"
dataset = pandas.read_csv(
    url, sep=",", header=1, engine="python")

print(dataset.shape)
print(dataset)
print(dataset.describe())

X = dataset.drop(columns=['genre'])  # input data set
y = dataset['genre']  # output data set

# assign 20% of dataset as test data
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)

print(X)
print(y)

model = DecisionTreeClassifier()
model.fit(X_train, y_train)
predictions = model.predict(X_test)
score = accuracy_score(y_test, predictions)
print(score)  # depends on how the dataset is divided and if enough of each category is provided; a larger dataset would be required

# Use the graphviz extension to create the diagram
tree.export_graphviz(model, out_file='music_recommender.dot',
                     feature_names=['age', 'gender'], class_names=sorted(y.unique()), label='all', rounded=True, filled=True)
